# twing-loader
[![NPM version][npm-image]][npm-url] [![Coverage percentage][coveralls-image]][coveralls-url]

Webpack loader for Twig templates, based on [Twing](https://www.npmjs.com/package/twing).

## Installation

`npm install`

## Contributing

* Fork this repository
* Code
* Implement tests using [tape](https://github.com/substack/tape)
* Issue a pull request keeping in mind that all pull requests must reference an issue in the issue queue

[npm-image]: https://badge.fury.io/js/twing-loader.svg
[npm-url]: https://npmjs.org/package/twing-loader
[coveralls-image]: https://coveralls.io/repos/gitlab/nightlycommit/twing-loader/badge.svg
[coveralls-url]: https://coveralls.io/gitlab/nightlycommit/twing-loader
