import {TestCase} from "../test-case";
import {runTestSuite} from "../test-suite";
import {createEnvironment, createFilesystemLoader} from "twing";
import fs from "fs";

class FromTestCase extends TestCase {
    get entry() {
        return __dirname + '/entry.js';
    }

    get expected() {
        return `BAR
`;
    }
}

const environment = createEnvironment(createFilesystemLoader(fs));

runTestSuite('from tag', [
    new FromTestCase(environment)
]);
