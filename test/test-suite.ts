import {TestCase} from "./test-case";
import tape from "tape";

export const runTestSuite = (
    name: string,
    testCases: Array<TestCase>
) => {
    tape(name, (test) => {
        const promises: Promise<void>[] = [];

        for (const testCase of testCases) {
            promises.push(testCase.run(test));
        }

        return Promise.all(promises)
            .finally(test.end)
            .then();
    });
};
