import {TestCase} from "../test-case";
import {runTestSuite} from "../test-suite";
import {createEnvironment, createFilesystemLoader} from "twing";
import fs from "fs";

class SelfTestCase extends TestCase {
    get entry() {
        return __dirname + '/entry.js';
    }

    get expected() {
        return 'test/_self/index.twig';
    }
}

const environment = createEnvironment(createFilesystemLoader(fs));

runTestSuite('_self variable', [
    new SelfTestCase(environment),
]);
