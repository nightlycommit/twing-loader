import {TestCase} from "../test-case";
import {runTestSuite} from "../test-suite";
import {createEnvironment, createFilesystemLoader} from "twing";
import fs from "fs";

class SourceFunctionTestCase extends TestCase {
    get entry() {
        return __dirname + '/entry.js';
    }

    get expected() {
        return `FOO
FOO
FOO
===
BAR
FOO 2`;
    }
}

const environment = createEnvironment(createFilesystemLoader(fs));

runTestSuite('source function', [
    new SourceFunctionTestCase(environment)
]);
