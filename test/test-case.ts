import {resolve as resolvePath} from 'path';
import {type Test} from "tape";
import webpack from "webpack";
import {type Configuration} from "webpack";
import MemoryFileSystem = require("memory-fs");
import type {TwingLoaderOptions} from "../src/lib/loader";
import type {TwingEnvironment} from "twing";

let moduleKey: string = require.resolve('../dist/index.js');

export abstract class TestCase {
    constructor(
        private readonly environment: TwingEnvironment
    ) {
    }

    abstract get expected(): string;

    abstract get entry(): string;

    get configuration(): Configuration {
        return {
            entry: resolvePath(this.entry),
            mode: 'none',
            module: {
                rules: [
                    {
                        test: /\.twig$/,
                        use: [
                            {
                                loader: moduleKey,
                                options: <TwingLoaderOptions>{
                                    environment: this.environment
                                }
                            }
                        ]
                    }
                ]
            },
            resolve: {
                fallback: {
                    buffer: require.resolve("buffer"),
                    path: require.resolve("path-browserify"),
                    stream: require.resolve("stream-browserify")
                }
            },
            output: {
                library: "template"
            }
        };
    }

    protected compile(configuration: Configuration): Promise<MemoryFileSystem> {
        return new Promise((resolve, reject) => {
            const compiler = webpack(configuration);

            let memoryFs = new MemoryFileSystem();

            compiler.outputFileSystem = memoryFs;

            compiler.run((err, stats) => {
                if (stats) {
                    if (stats.hasErrors()) {
                        reject(new Error(stats.toString("errors-only")));
                    }
                    else {
                        resolve(memoryFs);
                    }
                }
                else {
                    reject(err);
                }
            });
        });
    }

    run(test: Test): Promise<void> {
        let configuration: Configuration = this.configuration;

        delete require.cache[moduleKey];

        return this.compile(configuration)
            .then((memoryFs: MemoryFileSystem) => {
                return this.doTest(test, memoryFs);
            })
            .catch((err) => {
                test.fail(err.message);
            });
    }

    protected async doTest(test: Test, memoryFs: MemoryFileSystem): Promise<void> {
        const body = memoryFs.readFileSync(resolvePath('dist/main.js'), 'UTF-8');
        const candidate: string = await new Function(`${body}; return template.default;`)();

        test.same(candidate, this.expected);
    }
}
