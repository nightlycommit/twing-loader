import {TestCase} from "../test-case";
import {runTestSuite} from "../test-suite";
import {createEnvironment, createFilesystemLoader} from "twing";
import fs from "fs";

class EmbedTestCase extends TestCase {
    get entry() {
        return __dirname + '/entry.js';
    }

    get expected() {
        return `FOO
BAR`;
    }
}

const environment = createEnvironment(createFilesystemLoader(fs));

runTestSuite('embed tag', [
    new EmbedTestCase(environment),
]);
