import {TestCase} from "../../test-case";
import {runTestSuite} from "../../test-suite";
import {createEnvironment, createFilesystemLoader} from "twing";
import fs from "fs";

class IncludeFunctionTestCase extends TestCase {
    get entry() {
        return __dirname + '/entry.js';
    }

    get expected() {
        return `FOO
FOO
FOO
===
BAR
FOO 2
FOO`;
    }
}

const environment = createEnvironment(createFilesystemLoader(fs));

runTestSuite('include function', [
    new IncludeFunctionTestCase(environment)
]);
