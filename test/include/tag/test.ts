import {TestCase} from "../../test-case";
import {runTestSuite} from "../../test-suite";
import {createEnvironment, createFilesystemLoader} from "twing";
import fs from "fs";

class IncludeTagTestCase extends TestCase {
    get entry() {
        return __dirname + '/entry.js';
    }

    get expected() {
        return `FOOFOOFOO===
BARFOO 2FOO===
BAR`;
    }
}

const environment = createEnvironment(createFilesystemLoader(fs));

runTestSuite('include tag', [
    new IncludeTagTestCase(environment)
]);
