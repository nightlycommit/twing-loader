import template from './index.twig';
import {createEnvironment, createArrayLoader} from "twing";

export default template.render(
  createEnvironment(createArrayLoader({
    bar: 'BAR FROM THE ARRAY LOADER'
  })),
  {
    foo: 'BAR'
  }
);