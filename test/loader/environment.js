const {
  createEnvironment,
  createChainLoader,
  createFilesystemLoader
} = require("twing");
const fs = require("fs");

module.exports = createEnvironment(
  createChainLoader([
    createFilesystemLoader(fs)
  ])
);