import {TestCase} from "../test-case";
import {runTestSuite} from "../test-suite";
import {createEnvironment, createFilesystemLoader} from "twing";
import fs from "fs";

class ArrayLoaderTestCase extends TestCase {
    get entry() {
        return __dirname + '/entry.js';
    }

    get environmentModulePath(): string {
        return __dirname + '/environment.js';
    }

    get expected() {
        return `BAR FROM THE FS LOADER
BAR FROM THE ARRAY LOADER
`;
    }
}

const environment = createEnvironment(createFilesystemLoader(fs));

runTestSuite('with an array loader', [
    new ArrayLoaderTestCase(environment)
]);
