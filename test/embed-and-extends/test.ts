import {TestCase} from "../test-case";
import {runTestSuite} from "../test-suite";
import {createEnvironment, createFilesystemLoader} from "twing";
import fs from "fs";

class ExtendsTestCase extends TestCase {
    get entry() {
        return __dirname + '/entry.js';
    }

    get expected() {
        return `    <div class="header">
                    Skeleton header
            </div>
    <div class="content">
        BAR

    </div>
    <div class="footer">
                Custom footer
            <div class="header">
                    Skeleton header
            </div>
    <div class="content">
        BAR

    </div>
    <div class="footer">
                    Skeleton footer
            </div>

        </div>
`;
    }
}

const environment = createEnvironment(createFilesystemLoader(fs));

runTestSuite('embed and extends tag altogether', [
    new ExtendsTestCase(environment),
]);
