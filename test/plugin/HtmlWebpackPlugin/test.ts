import {TestCase} from "../../test-case";
import {Test} from "tape";
import {resolve as resolvePath} from "path";
import MemoryFileSystem = require("memory-fs");
import {runTestSuite} from "../../test-suite";
import {createEnvironment, createFilesystemLoader} from "twing";
import fs from "fs";

const HtmlWebpackPlugin = require('html-webpack-plugin');

class HtmlWebpackPluginTestCase extends TestCase {
    get configuration() {
        let configuration = super.configuration;

        configuration.plugins = [
            new HtmlWebpackPlugin({
                template: resolvePath(__dirname, 'template.js'),
                templateParameters: {
                    foo: 'BAR'
                },
                minify: false
            })
        ];

        return configuration;
    }

    get entry() {
        return __dirname + '/entry.js';
    }

    get expected() {
        return `FOO`;
    }

    get expectedFromPlugin() {
        return `<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>BAR</title>
<script defer src="main.js"></script></head>
<body>
</body>
</html>
`;
    }

    protected doTest(test: Test, memoryFs: MemoryFileSystem): Promise<void> {
        return super.doTest(test, memoryFs).then(() => {
            let actual = memoryFs.readFileSync(resolvePath('dist/index.html'), 'UTF-8');

            test.same(actual, this.expectedFromPlugin, 'plugin output is valid');
        });
    }
}

const environment = createEnvironment(createFilesystemLoader(fs));

runTestSuite('coupled with HtmlWebpackPlugin', [
    new HtmlWebpackPluginTestCase(environment),
]);
