import template from './index.twig';
import {createEnvironment, createArrayLoader} from "twing";

export default (context) => {
  return template.render(
    createEnvironment(createArrayLoader({})),
    context
  );
};