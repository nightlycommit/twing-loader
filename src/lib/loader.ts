import {type LoaderContext} from 'webpack';
import {
    createSource,
    type TwingEnvironment,
} from 'twing';
import {visit} from "./visitor";
import {existsSync} from "fs";
import {relative} from "path";

export type TwingLoaderOptions = {
    environment: TwingEnvironment;
};
export default function (this: LoaderContext<TwingLoaderOptions>, code: string) {
    const callback = this.async();

    const {environment} = this.getOptions();
    const {resourcePath} = this;

    const key = relative('.', resourcePath);
    const source = createSource(key, code);
    const tokenStream = environment.tokenize(source);
    const ast = environment.parse(tokenStream);

    visit(ast, environment, resourcePath).then((encounteredTemplates) => {
        let index = 0;

        const dependencies = encounteredTemplates
            .filter(([, resolvedName]) => existsSync(resolvedName))
            .map(([templateName, resolvedName]) => {
                return `import template${index} from "${resolvedName}";
templates.set('${templateName}', template${index++});`
            })
            .join('');

        const generatedCode = `import {createTemplate} from "twing";

const templates = new Map();

${dependencies}

const template = createTemplate(${JSON.stringify(ast)});
const baseLoadTemplate = template.loadTemplate;

template.loadTemplate = (executionContext, identifier) => {
    return templates.has(identifier) ? Promise.resolve(templates.get(identifier)) : baseLoadTemplate(executionContext, identifier);
};

for (const [, embeddedTemplate] of template.embeddedTemplates) {
    embeddedTemplate.loadTemplate = template.loadTemplate;
}

export default template;`;

        callback(null, generatedCode);
    });
};
