import {
    type TwingBaseNode,
    getChildren,
    type TwingConstantNode,
    type TwingEnvironment,
    type TwingTemplateNode,
    type TwingFunctionNode,
    type TwingConditionalNode,
    type TwingArrayNode,
    type TwingExpressionNode,
    type TwingImportNode,
    type TwingBlockFunctionNode, type TwingIncludeNode
} from "twing";
import {resolve} from "path";

type EncounteredTemplates = Array<[name: string, resolvedName: string]>;

export const visit = async (
    ast: TwingTemplateNode,
    environment: TwingEnvironment,
    id: string
): Promise<EncounteredTemplates> => {
    const encounteredTemplates: EncounteredTemplates = [];

    const registerTemplate = async (name: string) => {
        const templateFqn = await environment.loader.resolve(name, id);

        if (templateFqn !== null) {
            const resolvedName = resolve(templateFqn);

            if ((encounteredTemplates.find((encounteredTemplate) => encounteredTemplate[0] === name)) === undefined) {
                encounteredTemplates.push([name, resolvedName]);
            }
        }
    };

    const processExpressionNode = async (
        node: TwingExpressionNode
    ) => {
        if (node.type === "array") {
            const {children} = node as TwingArrayNode;

            for (const childName in children) {
                const index = Number.parseInt(childName);

                if (index % 2 === 1) {
                    await processExpressionNode(children[childName]);
                }
            }
        } else if (node.type === "conditional") {
            const {children} = node as TwingConditionalNode;

            await processExpressionNode(children.expr2);
            await processExpressionNode(children.expr3);
        } else if (node.type === "constant") {
            const templateName = (node as TwingConstantNode<string>).attributes.value;

            await registerTemplate(templateName);
        }
    };

    const visitNode = async (node: TwingBaseNode): Promise<void> => {
        for (const [, child] of getChildren(node)) {
            await visitNode(child);
        }

        if (node.type === "template") {
            if (node.children.parent) {
                await processExpressionNode(node.children.parent);
            }

            for (const embeddedTemplate of (node as TwingTemplateNode).embeddedTemplates) {
                await visitNode(embeddedTemplate);
            }
        }

        if (node.type === "include") {
            const {expression} = (node as TwingIncludeNode).children;

            await processExpressionNode(expression);
        }

        if (node.type === "import") {
            const {children} = node as TwingImportNode;

            if (children.templateName.type === "constant") {
                await processExpressionNode(children.templateName);
            }
        }

        if (node.type === "function" && ["include", "source"].includes((node as TwingFunctionNode).attributes.operatorName)) {
            const {arguments: argumentsNode} = (node as TwingFunctionNode).children;

            await processExpressionNode(argumentsNode);
        }

        if (node.type === "block_function") {
            const {children} = node as TwingBlockFunctionNode;

            if (children.template?.type === "constant") {
                await processExpressionNode(children.template);
            }
        }
    };

    return visitNode(ast).then(() => {
        return encounteredTemplates;
    });
};
