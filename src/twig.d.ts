declare module "*.twig" {
    import type {TwingTemplate} from "twing";

    const Template: {
        execute: TwingTemplate["execute"],
        render: TwingTemplate["render"]
    };

    export default Template;
}
