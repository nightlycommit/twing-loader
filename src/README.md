# twing-loader
[![NPM version][npm-image]][npm-url] [![Coverage percentage][coveralls-image]][coveralls-url]

Webpack loader for Twig templates, based on [Twing](https://www.npmjs.com/package/twing).

## Prerequisites

* Webpack 5

## Installation

`npm install twing-loader`

## Usage

`twing-loader` is a very straightforward Webpack loader, requiring only one option, the `environment` that is used to compile the templates.

<sub>webpack.config.mjs</sub>

```javascript
import {createEnvironment, createFilesystemLoader} from "twing";
import fs from "fs";

module.exports = {
    entry: 'index.js',
    // ...
    module: {
        rules: [
            {
                test: /\.twig$/,
                use: [
                    {
                        loader: 'twing-loader',
                        options: {
                            environment: createEnvironment(createFilesystemLoader(fs))
                        }
                    }
                ]
            }
        ]
    }
}
```

<sub>index.twig</sub>

```twig
{{ foo }}
```

<sub>index.mjs</sub>

```typescript
import {createEnvironment, createArrayLoader} from "twing";
import template from "./index.twig";
import assert from "assert";

const environment = createEnvironment(createArrayLoader({}));

template.render(environment, {
    foo: 'bar'
}).then((output) => {
    assert(output === "bar");
});
```

## Template signature

Twig templates imported using this loader are transformed into modules which default export has the following signature:

```typescript
interface Template {
    execute: import("twing").TwingTemplate["execute"];
    render: import("twing").TwingTemplate["render"];
}
```

Please refer to [Twing documentation](https://twing-api.nightlycommit.com/interfaces/TwingTemplate.html) for more information.

## Options

| Name        | Required | Type                               | Description                                                                          |
|-------------|----------|------------------------------------|--------------------------------------------------------------------------------------|
| environment | `true`   | `import("twing").TwingEnvironment` | The _environment_ instance used by the loader to compile the templates. |

[npm-image]: https://badge.fury.io/js/twing-loader.svg
[npm-url]: https://npmjs.org/package/twing-loader
[coveralls-image]: https://coveralls.io/repos/gitlab/nightlycommit/twing-loader/badge.svg
[coveralls-url]: https://coveralls.io/gitlab/nightlycommit/twing-loader
